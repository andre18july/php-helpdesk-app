<?php
  require_once('validador_acesso.php');

  unset($_SESSION['email']);
  session_destroy();
  header('Location: index.php');
?>