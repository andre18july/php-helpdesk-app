<?php

    session_start();

    

    $utilizadores = array(
        ['id' => 1, 'email' => 'andre@gmail.com', 'senha' => '12345', 'perfil_id' => 1],
        ['id' => 2, 'email' => 'tony@gmail.com', 'senha' => 'password', 'perfil_id' => 2],
        ['id' => 3, 'email' => 'joao@gmail.com', 'senha' => 'query', 'perfil_id' => 2]
    );

    $email = $_POST['email'];
    $senha = $_POST['senha'];


    $user_valido = false;
    $user_id = null;
    $perfil_id = null;

    $perfis = array(1 => 'Administrador', 2 => 'Utilizador');
    
    foreach($utilizadores as $utilizador){
        if($utilizador['email'] === $email && $utilizador['senha'] === $senha){
            $user_valido = true;
            $user_id = $utilizador['id'];
            $perfil_id = $utilizador['perfil_id'];
        }
    }

    if($user_valido){
        $_SESSION['email'] = $email;
        $_SESSION['id'] = $user_id;
        $_SESSION['perfil_id'] = $perfil_id;
        header("Location: home.php");
    }else{
        header("Location: index.php?login=erro");
    }

?>